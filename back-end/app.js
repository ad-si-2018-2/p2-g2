
var servidorirc = "ircd-hydri";

var express = require('express');  // módulo express
var app = express();		   // objeto express

var server = require('http').Server(app);
var io = require('socket.io')(server); //Módulo socket.io

var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');

var comandosSuportados = {
  join: "Use o comando join para entrar em uma sala. <br /> /join &#60canal&#62 <br/>",
  part: "Use o comando part para sair de um canal. <br /> /part <br />",
  kick: "Use o comando kick para expulsar um usuário de um canal(necessita privilégio de operador). <br /> /kick &#60canal&#62 &#60nick&#62<br />",
  motd: "Use o comando motd para exibir a mensagem do dia do servidor. <br /> /motd <br />",
  quit: "Use o comando quit para desconectar do servidor e de todos os canais. <br /> /quit <br /> ",
  help: "Use o comando help para exibir a lista de comandos suportados atualmente ou detalhes de determinado comando. <br /> /help &#91comando sem a &#47&#93 <br />",
  nick: "Use o comando nick para trocar de nick. <br /> /nick &#60nick&#62 <br />",
  invite: "Use o comando invite para convidar alguém para um canal. <br /> /invite &#60nick&#62 &#60canal&#62 <br />",
  mode: "Use o comando mode para mostrar os modos atuais do usuário. <br /> /mode &#60nick&#62 <br />",
  topic: "Use o comando topic para adicionar, remover ou exibir um tópico de um canal. <br /> /topic &#91topico&#93 <br />",
  clear: "Use o comando clear para limpar o mural de mensagens. <br /> /clear <br />"
}


app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

app.use('/gravar_mensagem', function(req, res, next) {

  if (!req.cookies.nick || !req.cookies.servidor || !req.cookies.id) {
      res.end(JSON.stringify({ response: "COOKIES_MISSING" }));
  }
  else {
      next();
  }
});

app.use('/alternarCanal/:canal', function(req, res, next) {
  if (!req.cookies.nick || !req.cookies.servidor || !req.cookies.id) {
      res.end(JSON.stringify({ response: "COOKIES_MISSING" }));
  }
  else {
      next();
  }
});

app.use('/quit', function(req, res, next) {
  if (!req.cookies.nick || !req.cookies.servidor || !req.cookies.id) {
      res.end(JSON.stringify({ response: "COOKIES_MISSING" }));
  }
  else {
      next();
  }
})

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;

/* Função de instanciação de proxy */
function proxy(id, servidor, nick, canal, callback) {

  var canais = {};

  var privMsg = {};

  var clientCache = [];

  if (canal && canal != 'client') canais[canal] = { clients: [], cache: [] };

  if (canal && canal != 'client') {
      var irc_client = new irc.Client(
          servidor,
          nick, { channels: [canal] });
  }
  else {
      var irc_client = new irc.Client(
          servidor,
          nick, { channels: [] });
  }


  irc_client.once('registered', function(message) {
      irc_client.opt.nick = message.args[0];
      console.log("Registrou no servidor irc");
      callback(message.args[0]);
  });

  irc_client.addListener('raw', function(message) {
      if (message.rawCommand == '001' || message.rawCommand == '002' || message.rawCommand == '003' || message.rawCommand == '251' || message.rawCommand == '252' || message.rawCommand == '253' || message.rawCommand == '254' || message.rawCommand == '255' || message.rawCommand == '265' || message.rawCommand == '250') {
          if (message.rawCommand == '252' || message.rawCommand == '253' || message.rawCommand == '254') {
              clientCache.push({
                  "timestamp": Date.now(),
                  "nick": servidorirc,
                  "msg": "<pre>" + message.args[1] + " " + message.args[2] + "</pre>"
              });
          }
          else if (message.rawCommand == '265') {
              clientCache.push({
                  "timestamp": Date.now(),
                  "nick": servidorirc,
                  "msg": "<pre>" + message.args[3] + "</pre>"
              });
          }
          else {
              clientCache.push({
                  "timestamp": Date.now(),
                  "nick": servidorirc,
                  "msg": "<pre>" + message.args[1] + "</pre>"
              });
          }
      };
  });

  console.log("Registrando proxy com id " + id);

  proxies[id] = { "irc_client": irc_client, "canais": canais, clientCache, "privMsg": privMsg }; //Adiciona ao mapa de proxies o novo proxy

  return proxies[id];
}


/* Get raíz */
app.get('/', function(req, res) {

  if (req.cookies.servidor && req.cookies.nick && req.cookies.loginStatus != "INVALID_CHANNEL") { //Testa se os cookies foram configurados com sucesso

      if (proxies[req.cookies.id]) { //Se já existe um proxy com o id do usuário

          res.sendFile(path.join(__dirname, "/index.html"));

      }
      else { //Se não tem, instanciar um novo proxy

          console.log("Instanciando um novo proxy");

          proxy_id = Math.floor((Math.random() * 1000) + 1);

          var p = proxy(proxy_id,
              req.cookies.servidor,
              req.cookies.nick,
              req.cookies.canal,
              function(nick) {
                  /*Callback executada quando o registro no servidor irc é conclúido */

                  if (req.cookies.nick != nick) { // Se o nick que o servidor cadastrou é diferente do que o usuário passou...

                      /* Desconecta do servidor irc */
                      proxies[proxy_id].irc_client.disconnect(function() {

                          /*Adiciona o cookie de status de login com o valor NICK_IN_USE */
                          res.cookie("loginStatus", "NICK_IN_USE");

                          /*Deleta o proxy gerado */
                          delete proxies[proxy_id];

                          /*Limpa os cookies configurados */
                          res.clearCookie("nick");
                          res.clearCookie("canal");
                          res.clearCookie("servidor");
                          res.clearCookie("id");

                          /*Envia o login.html para o client web */
                          res.sendFile(path.join(__dirname, '/login.html'));

                      });
                  }
                  else { // O nick é igual ao que o client passou

                      /*Limpa o cookie de loginStatus */
                      if (req.cookies.loginStatus) {
                          res.clearCookie("loginStatus");
                      }

                      /*Configurando cookie de id */
                      res.cookie("id", proxy_id);

                      /*Envia o index.html */
                      res.sendFile(path.join(__dirname, '/index.html'), function() {

                          /*Listener de conexão com o socket */
                          io.on("connection", function(socket) {

                              /*Atribui o socket a uma propriedade do proxy */
                              proxies[proxy_id].ws = socket;

                              /*Listeners client irc */
                              if (proxies[proxy_id].irc_client.listenerCount('message') < 1) {

                                  console.log("Configurando listeners...");

                                  proxies[proxy_id].irc_client.addListener('message', function(nick, to, message) {
                                      if (proxies[proxy_id].canais[to]) {
                                          proxies[proxy_id].canais[to]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": nick,
                                              "msg": message
                                          });

                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[to]["cache"], canal: to });
                                      }
                                      else { //Mensagem privada
                                          if (!proxies[proxy_id].privMsg[nick]) {
                                              proxies[proxy_id].privMsg[nick] = { cache: [] };
                                          }
                                          proxies[proxy_id].privMsg[nick]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": nick,
                                              "msg": message
                                          });
                                          proxies[proxy_id].ws.emit("mensagemPrivada", nick);
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].privMsg[nick]["cache"], canal: "-" + nick });
                                      }
                                  });

                                  proxies[proxy_id].irc_client.addListener('join', function(channel, nick, message) {
                                      console.log(nick + " entrou no canal " + channel);
                                      proxies[proxy_id].canais[channel]["cache"].push({
                                          "timestamp": Date.now(),
                                          "nick": servidorirc,
                                          "msg": nick + " entrou no canal."
                                      });
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });
                                      proxies[proxy_id].irc_client.send("names", channel);

                                  });

                                  proxies[proxy_id].irc_client.addListener('error', function(message) {
                                      if (message.rawCommand == '482') { //Mensagem de erro : Você não é operador
                                          proxies[proxy_id].canais[message.args[1]]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": message.args[2]
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[message.args[1]]["cache"], canal: message.args[1] });
                                      }

                                      if (message.rawCommand == '401') {
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "O nick " + message.args[1] + " não existe."
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: 'client' });
                                      }

                                      if (message.rawCommand == '403') {
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "O canal " + message.args[1] + " não existe."
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: 'client' });
                                      }

                                      console.log(message);
                                  });

                                  proxies[proxy_id].irc_client.addListener('+mode', function(channel, by, mode, argument) {
                                      console.log("Modo foi alterado +");
                                      if (!by) { // Não é um usuário colocando modo
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": servidorirc + " colocou modo +" + mode + " " + channel
                                          });
                                      }
                                      else { // É um usuário colocando modo
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": by,
                                              "msg": by + " colocou modo +" + mode + " " + argument
                                          });
                                      }
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });
                                  });

                                  proxies[proxy_id].irc_client.addListener('-mode', function(channel, by, mode, argument) {
                                      console.log("Modo foi alterado -");
                                      if (!by) { // Não é um usuário colocando modo
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": servidorirc + " colocou modo -" + mode + " " + channel
                                          });
                                      }
                                      else { // É um usuário colocando modo   
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": by,
                                              "msg": by + " colocou modo -" + mode + " " + argument
                                          });
                                      }
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });
                                  });

                                  proxies[proxy_id].irc_client.addListener('motd', function(motd) {
                                      proxies[proxy_id].clientCache.push({
                                          "timestamp": Date.now(),
                                          "nick": servidorirc,
                                          "msg": "<pre>" + motd + "</pre>"
                                      });
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: "client" });
                                  });

                                  proxies[proxy_id].irc_client.addListener('names', function(channel, nicks) {
                                      delete proxies[proxy_id].canais[channel]["clients"];
                                      proxies[proxy_id].canais[channel]["clients"] = [];
                                      for (var nick in nicks) {
                                          proxies[proxy_id].canais[channel]["clients"].push(nicks[nick] + nick);
                                      };
                                      proxies[proxy_id].ws.emit("nomesCanal", { clients: proxies[proxy_id].canais[channel]["clients"], canal: channel });
                                  });

                                  proxies[proxy_id].irc_client.addListener('part', function(channel, nick, reason) {
                                      if (nick != proxies[proxy_id].irc_client.opt.nick) { //Checa se o evento de part não é do próprio usuário
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": nick + " saiu do canal" + (reason != undefined ? "(" + reason + ")" : "")
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });
                                          proxies[proxy_id].irc_client.send("names", channel);
                                      }
                                      else {
                                          proxies[proxy_id].ws.emit("fecharAba", channel);
                                      }
                                  });

                                  proxies[proxy_id].irc_client.addListener('kick', function(channel, nick, by, reason) {
                                      if (nick == proxies[proxy_id].irc_client.opt.nick) { // O próprio usuário foi kickado
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "Você foi expulso do canal " + channel + "por " + by + (reason != undefined ? "(" + reason + ")" : "")
                                          });
                                          proxies[proxy_id].ws.emit("sairDoCanal", channel);
                                      }
                                      else {
                                          proxies[proxy_id].canais[channel]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": nick + " foi expulso do canal por " + by + (reason != undefined ? "(" + reason + ")" : "")
                                          });
                                          proxies[proxy_id].canais[channel]["clients"].splice(proxies[proxy_id].canais[channel]["clients"].indexOf(nick), 1);
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });
                                          proxies[proxy_id].irc_client.send("names", channel);
                                      }
                                  });

                                  proxies[proxy_id].irc_client.addListener('notice', function(nick, to, text, message) {
                                      if (!nick) {
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "<pre>" + text + "</pre>"
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: "client" })
                                      }
                                  });

                                  proxies[proxy_id].irc_client.addListener('nick', function(oldnick, newnick, channels) {
                                      console.log("Mudou de " + oldnick + " para " + newnick + " nos canais " + channels);
                                      channels.forEach(function(canal) {
                                          proxies[proxy_id].canais[canal]["cache"].push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "<pre>" + oldnick + " é conhecido como " + newnick + " agora.</pre>"
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[canal]["cache"], canal: canal });
                                          proxies[proxy_id].irc_client.send("names", canal);
                                      });
                                      if (proxies[proxy_id].irc_client.opt.nick == oldnick) {
                                          proxies[proxy_id].irc_client.opt.nick = newnick;
                                          proxies[proxy_id].ws.emit("mudarCookieNick", newnick);
                                      };
                                  });

                                  proxies[proxy_id].irc_client.addListener('invite', function(channel, from) {
                                      proxies[proxy_id].clientCache.push({
                                          "timestamp": Date.now(),
                                          "nick": servidorirc,
                                          "msg": "Você foi convidado por " + from + " para o canal <a onclick=" + "'entrarCanal(" + '"' + channel + '"' + ")'>" + channel + "</a>"
                                      });
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: 'client' });
                                  });

                                  proxies[proxy_id].irc_client.addListener('raw', function(message) {
                                      if (message.rawCommand == '221') {
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "Modos para " + message.args[0] + " : " + message.args[1]
                                          });
                                          proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: 'client' });
                                      }
                                  });

                                  proxies[proxy_id].irc_client.addListener('topic', function(channel, topic, nick, message) {
                                      proxies[proxy_id].canais[channel]["cache"].push({
                                          "timestamp": Date.now(),
                                          "nick": servidorirc,
                                          "msg": (nick != proxies[proxy_id].irc_client.opt.nick ? nick + " alterou o tópico para: " + topic : "Tópico: " + topic)
                                      });
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].canais[channel]["cache"], canal: channel });

                                  });

                                  proxies[proxy_id].irc_client.addListener('channellist', function(channel_list) {
                                      for (var i = 0, len = channel_list.length; i < len; i++) {
                                          proxies[proxy_id].clientCache.push({
                                              "timestamp": Date.now(),
                                              "nick": servidorirc,
                                              "msg": "<a onclick=" + "'entrarCanal(" + '"' + channel_list[i].name + '"' + ")'>" + channel_list[i].name + "</a> " + (channel_list[i].topic ? channel_list[i].topic : "")
                                          });
                                      }
                                      proxies[proxy_id].ws.emit("mensagemCanal", { msg: proxies[proxy_id].clientCache, canal: "client" });
                                  });

                              }

                              /*Listeners web socket */
                              proxies[proxy_id].ws.on("solicitaListaCanais", function() {
                                  proxies[proxy_id].ws.emit("listaCanais", { canais: proxies[proxy_id].canais, privMsg: proxies[proxy_id].privMsg });
                              });

                              proxies[proxy_id].ws.on("trocarMode", function(args) {
                                  proxies[proxy_id].ws.send("mode", args.user, args.mode);
                              });

                              /*Client irc criado, socket conectado = login bem sucedido */
                              proxies[proxy_id].ws.emit("successLogin");

                          });
                      });
                  }
              });

      }
  }
  else {
      res.sendFile(path.join(__dirname, '/login.html'));
  }
});

/* Post que trata mensagens vindas do client */
app.post('/gravar_mensagem', function(req, res) {

  var irc_client = proxies[req.cookies.id].irc_client; //Pega referência do client do proxy atual

  if (req.body.msg.charAt(0) == '/') { // Testa se é um comando

      /*Tratamento da string do comando */
      var command = req.body.msg.substring(1);
      var args = command.split(" ");

      /* Análise da string do comando */
      switch (args[0]) {
          case "motd": //Message of the day
              irc_client.send("motd");
              res.end();
              break;

          case "join": //Comando JOIN

              if (!args[1]) {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comando join faltando parâmetros <br /> Detalhes do comando join: <br /> " + comandosSuportados["join"] + "</pre>"
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }
              else if (args[1].charAt(0) != "#") {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": args[1] + " Canal inválido."
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }
              else if (!proxies[req.cookies.id].canais[args[1]]) { //Se o usuário ainda não está nesse canal

                  irc_client.join(args[1]); //Enviando comando para o servidor IRC

                  proxies[req.cookies.id].canais[args[1]] = { clients: [], cache: [] }; //Adiciona o canal criado na lista de canais

                  proxies[req.cookies.id].ws.emit("criarNovaAba", args[1]); //Acionando o socket do client para criar a aba do novo canal

              }

              res.end(); //Finaliza requisição sem retorno de dados
              break;

          case "part": //Comando PART

              if (proxies[req.cookies.id].canais[req.cookies.canal]) { //Se o usuário está no canal

                  delete proxies[req.cookies.id].canais[req.cookies.canal]; //Deleta o canal do array de canais do proxy

                  irc_client.part(req.cookies.canal); //Comando PART enviado para o servidor irc 

                  proxies[req.cookies.id].ws.emit("sairDoCanal", req.cookies.canal); //Acionando o socket do client para tratar a saída do canal

                  res.cookie("canal", 'client');

              }
              else {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "Canal inválido."
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }

              res.end(); //Finaliza requisição sem retorno de dados
              break;

          case "kick": //Comando KICK
              if (!args[1] || !args[2]) {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comando faltando parâmetros. <br /> Detalhes do comando kick: <br /> " + comandosSuportados["kick"] + "</pre>"
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });

              }
              else if (proxies[req.cookies.id].canais[req.cookies.canal]) { //Se o canal passado está entre os canais que o usuário está
                  if (!args[3]) //Se não tem mensagem do motivo
                      irc_client.send("kick", req.cookies.canal, args[2]);
                  else //Com mensagem do motivo
                      irc_client.send("kick", req.cookies.canal, args[2], args[3]);
              }

              res.end(); //Finaliza requisição sem retorno de dados
              break;

          case "quit": //Comando QUIT

              res.redirect('/quit'); //Redireciona para tratamento rota /quit
              break;

          case "help": //Comando HELP
              console.log(args);
              if (!args[1]) { //Apenas o /help
                  var comandos = "";
                  for (comando in comandosSuportados) {
                      comandos = comandos + "/" + comando + "<br />";
                  }

                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comandos disponíveis: <br/> " + comandos + "</pre>"
                  });
              }
              else { // /help comando
                  if (comandosSuportados[args[1]]) {
                      proxies[req.cookies.id].clientCache.push({
                          "timestamp": Date.now(),
                          "nick": servidorirc,
                          "msg": comandosSuportados[args[1]]
                      });
                  }
                  else { //Comando não encontrado
                      proxies[req.cookies.id].clientCache.push({
                          "timestamp": Date.now(),
                          "nick": servidorirc,
                          "msg": "<pre>" + args[1] + " Help não encontrado.</pre>"
                      });
                  }
              }
              proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: "client" });
              res.end();
              break;

          case "nick": //Comando NICK
              var patt = /\W/g;
              if (args[1] && !patt.test(args[1])) {
                  proxies[req.cookies.id].irc_client.send("nick", args[1]);
              }
              else {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comando faltando parâmetros. <br /> Detalhes do comando nick: <br /> " + comandosSuportados["nick"] + "</pre>"
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }
              res.end();
              break;
          case "invite": //Comando INVITE
              if (!args[1] || !args[2]) { //Faltando algum parâmetro
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comando faltando parâmetros. <br /> Detalhes do comando invite: <br /> " + comandosSuportados["invite"] + "</pre>"
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }
              else {
                  proxies[req.cookies.id].irc_client.send("invite", args[1], args[2]);
              }
              res.end();
              break;
          case "mode": //Comando MODE
              if (!args[1]) {
                  proxies[req.cookies.id].clientCache.push({
                      "timestamp": Date.now(),
                      "nick": servidorirc,
                      "msg": "<pre>Comando faltando parâmetros. <br />Detalhes do comando mode: <br /> " + comandosSuportados["mode"] + "</pre>"
                  });
                  proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: 'client' });
              }
              else {
                  proxies[req.cookies.id].irc_client.send("mode", args[1]);
              }
              res.end();
              break;
          case "topic": //Comando TOPIC
              if (proxies[req.cookies.id].canais[req.cookies.canal]) {
                  if (!args[1]) proxies[req.cookies.id].irc_client.send("topic", req.cookies.canal);
                  else {
                      var topico = "";
                      for (var i = 1, len = args.length; i < len; i++) {
                          topico = topico + args[i] + (len == args.length - 1 ? "" : " ");
                      }
                      proxies[req.cookies.id].irc_client.send("topic", req.cookies.canal, topico);
                  }
              }
              res.end();
              break;
          case "list": //Comando LIST
              proxies[req.cookies.id].irc_client.send("list");
              res.end();
              break;
          case "clear": //Comando CLEAR
              switch (req.cookies.canal) {
                  case "client":
                      proxies[req.cookies.id].clientCache = [];
                      proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: "client" });
                      break;
                  default:
                      proxies[req.cookies.id].canais[req.cookies.canal]["cache"] = [];
                      proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].canais[req.cookies.canal]["cache"], canal: req.cookies.canal });
              }
              res.end();
              break;
          default: //Comando desconhecido

              proxies[req.cookies.id].clientCache.push({
                  "timestamp": Date.now(),
                  "nick": servidorirc,
                  "msg": args + " Comando desconhecido."
              });
              proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: "client" });
              res.end();
              break;
      }
  }
  else { //Não é um comando
      console.log("Não é um comando");
      if (proxies[req.cookies.id].canais[req.cookies.canal]) { //Mensagem para canal?

          irc_client.say(req.cookies.canal, req.body.msg);
          proxies[req.cookies.id].canais[req.cookies.canal]["cache"].push(req.body);
          proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].canais[req.cookies.canal]["cache"], canal: req.cookies.canal });

      }
      else if (req.cookies.canal == "client") { //Mensagem para o servidor?

          console.log("Tentativa de comando mal sucedida");
          proxies[req.cookies.id].clientCache.push({
              "timestamp": Date.now(),
              "nick": servidorirc,
              "msg": req.body.msg + " Comando desconhecido."
          });
          proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: req.cookies.canal });

      }
      else { //Mensagem privada?
          irc_client.say(req.cookies.canal.substring(1), req.body.msg);
          proxies[req.cookies.id].privMsg[req.cookies.canal.substring(1)]["cache"].push(req.body);
          proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].privMsg[req.cookies.canal.substring(1)]["cache"], canal: req.cookies.canal });

      }

      res.end();
  }

});

/*Get que trata da solicitação de mensagens de determinado canal */
app.get('/alternarCanal', function(req, res) {
  console.log("Aba requisitada: " + req.cookies.canal);
  if (req.cookies.canal == 'client') { //Pedindo mensagens do servidor
      proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].clientCache, canal: req.cookies.canal });
  }
  else if (proxies[req.cookies.id].canais[req.cookies.canal]) { //Pedindo mensagem de um canal

      proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].canais[req.cookies.canal]["cache"], canal: req.cookies.canal });
      proxies[req.cookies.id].irc_client.send("names", req.cookies.canal);
  }
  else { //Pedindo mensagens de uma conversa privada

      /*Caso o objeto cache de mensagens daquela mensagem privada ainda não existe */
      if (!proxies[req.cookies.id].privMsg[req.cookies.canal.substring(1)]) {
          proxies[req.cookies.id].privMsg[req.cookies.canal.substring(1)] = { cache: [] };
      }

      proxies[req.cookies.id].ws.emit("mensagemCanal", { msg: proxies[req.cookies.id].privMsg[req.cookies.canal.substring(1)]["cache"], canal: req.cookies.canal });
      proxies[req.cookies.id].ws.emit("nomesCanal", { clients: [], canal: req.cookies.canal });

  }

  res.end();
});

/*Get que trata o login de um novo usuário */
app.post('/login', function(req, res) {

  /* Configurando os cookies do client */
  if (req.body.canal) {
      if (req.body.canal.charAt(0) != '#') {
          res.cookie('loginStatus', "INVALID_CHANNEL");
      }
      else {
          if (res.cookie('loginStatus')) res.clearCookie('loginStatus');
          res.cookie('canal', req.body.canal);
      }
  }
  else {
      if (res.cookie('loginStatus')) res.clearCookie('loginStatus');
  }

  var patt = /\W/g;

  if (patt.test(req.body.nome)) {
      res.cookie('loginStatus', "INVALID_NAME");
  }
  else {
      res.cookie('nick', req.body.nome);
  }

  res.cookie('servidor', req.body.servidor);
  res.redirect('/');

});

/* Get que trata a saída de um client */
app.get('/quit', function(req, res) {

  for (canal in proxies[req.cookies.id].canais) {
      proxies[req.cookies.id].irc_client.part(canal);
  };

  proxies[req.cookies.id].irc_client.disconnect(function() { //Desconecta do servidor irc


      console.log("Desconectando...");

      /* Limando os cookies */
      res.clearCookie("nick");
      res.clearCookie("canal");
      res.clearCookie("servidor");
      res.clearCookie("loginStatus");

      proxies[req.cookies.id].ws.disconnect(true); // Desconecta o socket


      delete proxies[req.cookies.id]; //Apaga do mapa de proxies

      res.clearCookie("id");

      /*Encerrando a requisição */
      res.end();

  });

});



app.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});
